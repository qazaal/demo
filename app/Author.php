<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Author extends Model implements JWTSubject
{
    protected $primaryKey='id';

    protected $fillable=['firstName','lastName','email','password','avatar'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function books()
    {
         return $this->hasMany(Book::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
