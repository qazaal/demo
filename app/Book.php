<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table="books";
    protected $primaryKey='id';
    protected $fillable=['book_name','number_of_page','publisher','author_id'];


    public function author()
    {
        $this->belongsTo(Author::class,'author_id');

    }


}
