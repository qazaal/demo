<?php

namespace App\Http\Controllers;

use App\author;
use App\Book;
use App\Http\Requests\AuthorRequest;
use App\Jobs\SendEmail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class AuthorController extends Controller
{


    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $token = null;

        if (!$token = auth('author')->attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'user_id' => auth('api')->user()->id,
            'token' => $token,
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Author::all();
        return response()->json([
            'success'   =>  true,
            'data'      =>  $list
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        $user = new Author();
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();


        $image=$request->file('avatar');
        $path=$this->uploadImage($image);
        $user->update(['avatar'=>$path]);
        SendEmail::dispatch(['email'=>'admin@site.com']);

        return response()->json([
            'success'   =>  true,
            'data'      =>  $user
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author=Author::findOrFail($id);
        return response()->json([
            'success'   =>  true,
            'data'      =>  $author
        ], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $author=Author::findOrFail($id);
        if (!$author){
            return response('not found',404);

        }

        $author->fill($request->except(['password','avatar']));
        if ($request->has('password')){
            $author->password=bcrypt($request->password);
        }
        if($author->save()){
            if ($request->file('avatar')){
                $image=$request->file('avatar');
                $avatar_path=$this->uploadImage($image);
                $author->avatar=$avatar_path;
                $author->save();
            }
            return response()->json([
                'success' => true,
                'task' => $author
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Sorry, task could not be updated.'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author=Author::findOrFail($id);
        $author->delete();
        return response()->json([
            'success' => false,
            'message' => 'Sorry, task could not be updated.'
        ], 204);

    }

    /**
     * @param $image
     * @return JsonResponse|string
     * return image name of avatar
     */
    private function uploadImage($image)
    {

            $thumbnailPath = base_path().'/public';

            if (!File::exists($thumbnailPath)) {
                File::makeDirectory($thumbnailPath, 0777, true, true);
            }

            $imageName = time().'.'.$image->extension();
            $success=$image->move($thumbnailPath, $imageName);
            if (!$success){
                return response()->json([
                        'data' => null,
                        'status' => false,
                        'err_' => [
                            'message' => 'Image upload failed',
                            'code' => 1
                        ]
                    ]
                );

            }
            return $thumbnailPath.'/'.$imageName;
    }

    public function getAuthorWithBook()
    {
        $list=Author::with(['books'=> function ($query) {

        }])->latest()->paginate(10);
        return response()->json([
            'success'   =>  true,
            'data'      =>  $list
        ], 201);


    }
}
