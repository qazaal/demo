<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list= Book::all();
        return response()->json([
        'success'   =>  true,
        'data'      =>  $list
    ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validation = $request->validate([
            'book_name' => 'required|max:255',
            'number_of_page' => 'required',
            'publisher' => 'required|max:255',
            'author_id' => 'required',
        ]);
        if (!$validation){
            return response()->json([
                'success'   =>  true,
                'data'      =>  $validation->errors()
            ], 400);
        }
        $author=Author::findOrFail($request->author_id);
        $book=new Book();
        $book->fill($request->all());

        if ($book->save()){
            return response()->json([
                'success'   =>  true,
                'data'      =>  $book
            ], 201);
        }else{
            return response()->json([
                'success'   =>  true,
                'data'      =>  $request->all()
            ], 400);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book=Book::findOrFail($id);
        return response()->json([
            'success'   =>  true,
            'data'      =>  $book
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book=Book::findOrFail($id);
        $validation = $request->validate([
            'book_name' => 'sometimes|required|max:255',
            'number_of_page' => 'sometimes|required',
            'publisher' => 'sometimes|required|max:255',
            'author_id' => 'sometimes|required',
        ]);
        if (!$validation){
            return response()->json([
                'success'   =>  true,
                'data'      =>  $validation->errors()
            ], 400);
        }

        $book->fill($request->all());
        if ($book->save()){

                return response()->json([
                    'success'   =>  true,
                    'data'      =>  $book
                ], 200);

        }else{

                return response()->json([
                    'success'   =>  true,
                    'data'      =>  'not save'
                ], 400);

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book=Book::findOrFail($id);
        if ($book->delete()){
            return response()->json([
                'success'   =>  true,
                'data'      =>  1
            ], 204);
        }else{
            return response()->json([
                'success'   =>  true,
                'data'      =>  'failed delete'
            ], 400);
        }
    }
}
