<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Gate::allows('canAdd')){
            return response()->json([
                    'data' => null,
                    'status' => 403,
                    'err_' => [
                        'message' => 'your not permitted for crud this resource',
                        'code' => 1
                    ]
                ]
            );
        }
        return $next($request);
    }
}
