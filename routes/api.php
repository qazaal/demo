<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');

Route::post('authorLogin', 'Api\Admin\Auth\LoginController@login');



Route::group(['middleware' => ['jwt.verify','is_admin'] ], function () {

    Route::get('logout', 'ApiController@logout');
    Route::get('logoutAuthor', 'ApiController@logout');
    Route::get('author_with_books','AuthorController@getAuthorWithBook');
    Route::resource('books', 'BookController');
    Route::resource('authors', 'AuthorController');

});

